package TextField;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MyTextField extends Application {

	Stage MyWindow;
	Scene MyScene;
	Button MyButton;
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage)throws Exception {
		MyWindow = primaryStage;
		MyWindow.setTitle("CCAC");
		
		TextField MyTextBox = new TextField();
		MyButton = new Button("Submit");
		MyButton.setOnAction(e -> System.out.println(MyTextBox.getText()));
		
		
		//Set the layout
		VBox MyLayout = new VBox(10);
		MyLayout.setPadding(new Insets(10,10,10,10));
		MyLayout.getChildren().addAll(MyTextBox,MyButton);
		
		MyScene = new Scene(MyLayout, 300, 300);
		MyWindow.setScene(MyScene);
		MyWindow.show();

	}

}