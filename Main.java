package BasicWindow;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application implements EventHandler<ActionEvent> { //2

Button button; //initilize the button

public static void main(String[] args){
	launch(args);
}

@Override
public void start(Stage primaryStage)throws Exception{
	primaryStage.setTitle("Title of the Window");
	button = new Button(); //Creates the button
	button.setText("Submit");
	button.setOnAction(this);
	
	//Make a layout ( how you want everything arranged on the screen)
	StackPane layout = new StackPane();
	layout.getChildren().add(button);
	
	Scene scene = new Scene(layout, 300, 250); //Contents inside of the stage
	primaryStage.setScene(scene);
	primaryStage.show();
	
}
	@Override
	public void handle(ActionEvent event){
	  if(event.getSource()==button){
		  System.out.println("it works");
	  }
	}


}